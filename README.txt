Surround Game
Language: Java
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 163 - Java Programming 2
Semester / Year: Winter 2015

Surround is a game where you want to surround the other player's tiles with your own in order to win.

Usage:
Run the main method in 'Surround' to start the game and follow the GUI prompts to setup the game. Click the tiles to place peices

Rules:
Goal of the game: Surround your opponent's cell with your own.
Ways to surround your opponent:
Surround North, South, East, and West of your opponents. (See Ex. 1)
Surround 3 sides of your opponent if they're on the side. (See Ex. 2)
Surround 2 sides of your opponent if they're in the corner. (See Ex. 3)

** Player 2 wins all of the examples except Ex. 5. **
** 0's represent empty cells in these examples. **

Ex. 1
[0][0][0][0][0]
[0][0][2][0][0]
[0][2][1][2][0]
[0][0][2][0][0]
[0][0][0][0][0]

Ex. 2
[0][0][0][0][0]
[0][0][0][0][2]
[0][0][0][2][1]
[0][0][0][0][2]
[0][0][0][0][0]

Ex. 3
[1][2][0][0][0]
[2][0][0][0][0]
[0][0][0][0][0]
[0][0][0][0][0]
[0][0][0][0][0]


If there are more than two players, the one that places the last
cell to surround the opponent gets the win. (See Ex. 4)
Ex. 4
[0][0][0][0][0]
[0][0][2][0][0]
[0][4][1][3][0]	Player 2 wins for placing the last cell.
[0][0][4][0][0]
[0][0][0][0][0]

You can not lose by surrounding yourself. (See Ex. 5)
Ex. 5
[0][0][0][0][0]
[0][0][2][0][0]
[0][1][1][3][0]	Player 1 is not out, and game is still in progress.
[0][0][4][0][0]
[0][0][0][0][0]