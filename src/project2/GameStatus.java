package project2;

/********************************************************
 * An enum class containing all the possible states
 * the Surround game can be in: PLAYER1_WON, PLAYER2_WON,
 * PLAYER3_WON, PLAYER4_WON, TIE, IN_PROGRESS
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 *******************************************************/
public enum GameStatus {
	PLAYER1_WON, PLAYER2_WON, PLAYER3_WON, PLAYER4_WON, TIE, IN_PROGRESS
}
