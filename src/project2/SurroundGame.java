package project2;

import java.awt.Point;
import java.util.ArrayList;

/****************************************************************
 * This class manages all the functionality of the Surround game
 * by keeping track of player movements.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 ***************************************************************/
public class SurroundGame {

	// Initialize class variables
	private Cell[][] board;
	private int size, players, turn, firstTurn;
	private GameStatus status;
	/** List of moves that can be undone. */
	private ArrayList<Point> undoList = new ArrayList<Point>();
	
	/*************************************************************
	 * Constructs a new SurroundGame with size, players, and turn
	 * set to the passed parameters and board is instantiated.
	 * 
	 * @param size size of the board
	 * @param players amount of players playing
	 * @param turn what player goes first
	 ************************************************************/
	public SurroundGame(int size, int players, int turn) {
		this.size = size;
		this.players = players;
		this.turn = turn;
		firstTurn = turn;
		
		status = GameStatus.IN_PROGRESS;
		
		// Create the board with each spot set to 0 for empty
		board = new Cell[size][size];
		
		for (int r = 0; r < size; r++) {
			for (int c = 0; c < size; c++) {
				board[r][c] = new Cell();
			}
		}
	}
	
	/*********************************************************
	 * Checks to see if the cell the player selected is valid
	 * (not taken already) and if it is adds that cell to the 
	 * undoList and sets turn to the next player.
	 * 
	 * @param row row of the selected cell
	 * @param col column of the selected cell
	 * @return true if valid cell, false otherwise
	 ********************************************************/
	public Boolean select(int row, int col) {
		if (board[row][col].getPlayerNumber() == 0) {
			undoList.add(new Point(row, col));
			nextPlayer();
			return true;
		}
		
		return false;
	}
	
	/******************************************************
	 * Resets the board to empty and sets the turn back to
	 * the player assigned as for the first turn when game
	 * was first instantiated.
	 *****************************************************/
	public void reset() {
		for (int r = 0; r < size; r++) {
			for (int c = 0; c < size; c++) {
				board[r][c].setPlayerNumber(0);
			}
		}
		undoList.clear();
		turn = firstTurn;
	}
	
	/*******************************************************
	 * Undoes the previous move located in the undoList
	 * and removes that move from the list.
	 * 
	 * @return point containing the coordinates of the cell
	 * that was just deselected by the undo
	 ******************************************************/
	public Point undo() {
		Point p = undoList.remove(undoList.size() - 1);
		board[p.x][p.y].setPlayerNumber(0);
		previousPlayer();
		return p;
	}
	
	/***********************************
	 * Returns what player's turn it is.
	 * 
	 * @return player's turn
	 **********************************/
	public int getTurn() {
		return turn;
	}
	
	/*********************************************
	 * Returns the current GameStatus of the game.
	 * 
	 * @return current GameStatus
	 ********************************************/
	public GameStatus getStatus() {
		return status;
	}
	
	/***********************************************
	 * Sets the turn to the previous player's turn.
	 **********************************************/
	private void previousPlayer() {
		if (turn == 1) {
			turn = players;
		} else {
			turn--;
		}
	}
	
	/*******************************************
	 * Sets the turn to the next player's turn.
	 ******************************************/
	public void nextPlayer() {
		if (turn == players) {
			turn = 1;
		} else {
			turn ++;
		}
	}
	
	/**************************************************
	 * Sets the cell located at the corresponding row
	 * and cell that was passed to the current player.
	 * 
	 * @param row row of the cell
	 * @param col column of the cell
	 *************************************************/
	public void setCell(int row, int col) {
		board[row][col].setPlayerNumber(turn);
	}
	
	
	/***********************************************
	 * Checks the board to see if there is a winner
	 * and sets the game status accordingly.
	 **********************************************/
	public void isWinner() {
		for (int r = 0; r < size; r++) {
			for (int c = 0; c < size; c++) {
				if (checkMiddle(r, c) || checkCorners(r, c) || checkSides(r, c)) {
					whoWinner();
					return;
				}
			}
		}
		
		for (int r = 0; r < size; r++) {
			for (int c = 0; c < size; c++) {
				if (board[r][c].getPlayerNumber() == 0) {
					status = GameStatus.IN_PROGRESS;
					return;
				}
			}
		}
		
		status= GameStatus.TIE;
	}
	
	/**************************************************
	 * Checks who is the winner by looking at the
	 * previous turn and getting that player's number.
	 *************************************************/
	private void whoWinner() {
		previousPlayer();
		if (turn == 1) {
			status = GameStatus.PLAYER1_WON;
			return;
		}
		else if (turn == 2) {
			status = GameStatus.PLAYER2_WON;
			return;
		}
		else if (turn == 3) {
			status = GameStatus.PLAYER3_WON;
			return;
		}
		else if (turn == 4) {
			status = GameStatus.PLAYER4_WON;
			return;
		}
	}
	
	/****************************************************
	 * Checks the corners to see if the player in one of
	 * those spots has been surrounded.
	 * 
	 * @param row row of the cell being surrounded
	 * @param col column of the cell being surrounded
	 * 
	 * @return true if the cell is surrounded, false
	 * otherwise
	 ***************************************************/
	private Boolean checkCorners(int row, int col) {
		// Check top left corner
		if (row == 0 && col == 0) {
			if (board[row][col].getPlayerNumber() != 0) {
				int temp = board[row][col].getPlayerNumber();
				if (board[row][col + 1].getPlayerNumber() != temp &&
					board[row][col + 1].getPlayerNumber() != 0) {
					if (board[row + 1][col].getPlayerNumber() != temp &&
						board[row + 1][col].getPlayerNumber() != 0) {
						return true;
					}
				}
			}
		}
		
		// Check top right corner
		if (row == 0 && col == (size - 1)) {
			if (board[row][col].getPlayerNumber() != 0) {
				int temp = board[row][col].getPlayerNumber();
				if (board[row][col - 1].getPlayerNumber() != temp &&
					board[row][col - 1].getPlayerNumber() != 0) {
					if (board[row + 1][col].getPlayerNumber() != temp &&
						board[row + 1][col].getPlayerNumber() != 0) {
						return true;
					}
				}
			}
		}
		
		// Check bottom left corner
		if (row == (size - 1) && col == 0) {
			if (board[row][col].getPlayerNumber() != 0) {
				int temp = board[row][col].getPlayerNumber();
				if (board[row][col + 1].getPlayerNumber() != temp &&
					board[row][col + 1].getPlayerNumber() != 0) {
					if (board[row - 1][col].getPlayerNumber() != temp &&
						board[row - 1][col].getPlayerNumber() != 0) {
						return true;
					}
				}
			}
		}
		
		// Check bottom right corner
		if (row == (size - 1) && col == (size - 1)) {
			if (board[row][col].getPlayerNumber() != 0) {
				int temp = board[row][col].getPlayerNumber();
				if (board[row][col - 1].getPlayerNumber() != temp &&
					board[row][col - 1].getPlayerNumber() != 0) {
					if (board[row - 1][col].getPlayerNumber() != temp &&
						board[row - 1][col].getPlayerNumber() != 0) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	/****************************************************
	 * Checks the sides to see if the player in one of
	 * those spots has been surrounded.
	 * 
	 * @param row row of the cell being surrounded
	 * @param col column of the cell being surrounded
	 * 
	 * @return true if the cell is surrounded, false
	 * otherwise
	 ***************************************************/
	private Boolean checkSides(int row, int col) {
		// Check top
		if (row == 0 && col != 0 && col != (size - 1)) {
			if (board[row][col].getPlayerNumber() != 0) {
				int temp = board[row][col].getPlayerNumber();
				if (board[row][col - 1].getPlayerNumber() != temp &&
					board[row][col - 1].getPlayerNumber() != 0) {
					if (board[row][col + 1].getPlayerNumber() != temp &&
						board[row][col + 1].getPlayerNumber() != 0) {
						if (board[row + 1][col].getPlayerNumber() != temp &&
							board[row + 1][col].getPlayerNumber() != 0) {
							return true;
						}
					}
				}
			}
		}
		
		// Check bottom
		if (row == (size - 1) && col != 0 && col != (size - 1)) {
			if (board[row][col].getPlayerNumber() != 0) {
				int temp = board[row][col].getPlayerNumber();
				if (board[row][col - 1].getPlayerNumber() != temp &&
					board[row][col - 1].getPlayerNumber() != 0) {
					if (board[row][col + 1].getPlayerNumber() != temp &&
						board[row][col + 1].getPlayerNumber() != 0) {
						if (board[row - 1][col].getPlayerNumber() != temp &&
							board[row - 1][col].getPlayerNumber() != 0) {
							return true;
						}
					}
				}
			}
		}
		
		// Check left side
		if (row != 0 && row != (size - 1) && col == 0) {
			if (board[row][col].getPlayerNumber() != 0) {
				int temp = board[row][col].getPlayerNumber();
				if (board[row][col + 1].getPlayerNumber() != temp &&
					board[row][col + 1].getPlayerNumber() != 0) {
					if (board[row + 1][col].getPlayerNumber() != temp &&
						board[row + 1][col].getPlayerNumber() != 0) {
						if (board[row - 1][col].getPlayerNumber() != temp &&
							board[row - 1][col].getPlayerNumber() != 0) {
							return true;
						}
					}
				}
			}
		}
		
		// Check right side
		if (row != 0 && row != (size - 1) && col == (size - 1)) {
			if (board[row][col].getPlayerNumber() != 0) {
				int temp = board[row][col].getPlayerNumber();
				if (board[row][col - 1].getPlayerNumber() != temp &&
					board[row][col - 1].getPlayerNumber() != 0) {
					if (board[row + 1][col].getPlayerNumber() != temp &&
						board[row + 1][col].getPlayerNumber() != 0) {
						if (board[row - 1][col].getPlayerNumber() != temp &&
							board[row - 1][col].getPlayerNumber() != 0) {
							return true;
						}
					}
				}
			}
		}
		
		return false;
	}
	
	/****************************************************
	 * Checks the middle to see if the player in one of
	 * those spots has been surrounded.
	 * 
	 * @param row row of the cell being surrounded
	 * @param col column of the cell being surrounded
	 * 
	 * @return true if the cell is surrounded, false
	 * otherwise
	 ***************************************************/
	private Boolean checkMiddle(int row, int col) {
		if(row > 0 && row < (size - 1) && col > 0 && col < (size - 1)) {
			if (board[row][col].getPlayerNumber() != 0) {
				int temp = board[row][col].getPlayerNumber();
				if (board[row - 1][col].getPlayerNumber() !=  temp &&
				    board[row - 1][col].getPlayerNumber() != 0) {
					if (board[row + 1][col].getPlayerNumber() !=  temp &&
						board[row + 1][col].getPlayerNumber() != 0) {
						if (board[row][col - 1].getPlayerNumber() !=  temp &&
							board[row][col - 1].getPlayerNumber() != 0) {
							if (board[row][col + 1].getPlayerNumber() !=  temp &&
								board[row][col + 1].getPlayerNumber() != 0) {
								return true;
							}
						}
					}
				}
			}
		}
		
		return false;
	}
}
