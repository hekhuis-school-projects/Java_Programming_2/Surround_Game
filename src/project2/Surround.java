package project2;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/***********************************************
 * Creates and runs the Surround game by making
 * the main frame and the menu items to be
 * passed to the SurroundPanel class.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 **********************************************/
public class Surround {

	public static void main(String[] args) {
		JFrame frame = new JFrame("Surround");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Set up the menu bar for the frame
		JMenuBar menuBar = new JMenuBar();
		
		// Set up file menu
		JMenu fileMenu = new JMenu("File");
		JMenuItem newGameMenuItem = new JMenuItem("New Game");
		JMenuItem undoMenuItem = new JMenuItem("Undo");
		JMenuItem quitMenuItem = new JMenuItem("Quit");
		fileMenu.add(newGameMenuItem);
		fileMenu.add(undoMenuItem);
		fileMenu.add(quitMenuItem);
		
		// Set up help menu
		JMenu helpMenu = new JMenu("Help");
		JMenuItem rulesMenuItem = new JMenuItem("Rules");
		JMenuItem aboutMenuItem = new JMenuItem("About");
		helpMenu.add(rulesMenuItem);
		helpMenu.add(aboutMenuItem);
		
		// Add menus to menu bar
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);
		frame.setJMenuBar(menuBar);
		
		new SurroundPanel(frame, newGameMenuItem, undoMenuItem, quitMenuItem, rulesMenuItem, aboutMenuItem);
		
		// Center the frame on the desktop
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setVisible(true);
	}
}
