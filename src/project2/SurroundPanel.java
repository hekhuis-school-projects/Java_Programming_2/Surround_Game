package project2;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.LineBorder;

/*************************************************************
 * Represents the main GUI for the Surround game and contains
 * all the JPanels, JButtons, and JMenuItems for the game.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 ************************************************************/
@SuppressWarnings("serial")
public class SurroundPanel extends JPanel {

	// Initialize board size variables
	private static final int BOARD_SIZE_MIN = 3;
	private static final int BOARD_SIZE_MAX = 20;
	/** Size of the board */
	private int size;
	
	// Initialize amount of players (min, max, actual)
	private static final int PLAYERS_MIN = 2;
	private static final int PLAYERS_MAX = 4;
	/** Amount of players */
	private int players;
	
	// Initialize the game and game board
	private JButton[][] board;
	private SurroundGame game;
	
	// Initialize frame and menu
	private JFrame frame;
	private JMenuItem newGameMenuItem,
				      undoMenuItem,
				      quitMenuItem,
				      rulesMenuItem,
				      aboutMenuItem;
	
	// Initialize button listener
	private ButtonListener listener;
	
	// Initialize score labels for players 1 - 4
	private JLabel p1WinsLabel, 
				   p2WinsLabel,
				   p3WinsLabel,
				   p4WinsLabel;
	private int p1Wins,
			    p2Wins,
			    p3Wins,
			    p4Wins;
	
	// Initialize turn and turn label
	private JLabel turnLabel;
	/** What player's turn is it */
	private int turn;
	
	// Timer for player's turns
	private Countdown timer;
	
	/****************************************************
	 * Constructs a new SurroundPanel object with frame,
	 * newGameMenuItem, undoMenuItem, quitMenuItem,
	 * rulesMenuItem, and aboutMenuItem set to the given
	 * parameters. Also starts a timer allowing the
	 * player 15 seconds to choose a spot, or it moves
	 * onto the next player's turn.
	 * 
	 * @param frame frame of the window
	 * @param newGameMenuItem new game menu button
	 * @param undoMenuItem undo menu button
	 * @param quitMenuItem quit menu button
	 * @param rulesMenuItem rules menu button
	 * @param aboutMenuItem about menu button
	 ***************************************************/
	public SurroundPanel(JFrame frame, 
						 JMenuItem newGameMenuItem, 
						 JMenuItem undoMenuItem, 
						 JMenuItem quitMenuItem,
						 JMenuItem rulesMenuItem, 
						 JMenuItem aboutMenuItem) {
		this.frame = frame;
		this.newGameMenuItem = newGameMenuItem;
		this.undoMenuItem = undoMenuItem;
		this.quitMenuItem = quitMenuItem;
		this.rulesMenuItem = rulesMenuItem;
		this.aboutMenuItem = aboutMenuItem;
		
		size = getBoardSize();
		players = getAmountOfPlayers();
		turn = whoStarts();
		
		undoMenuItem.setEnabled(false);
		
		game = new SurroundGame(size, players, turn);
		
		// Set up the layout of the main panel as BorderLayout
		this.setLayout(new BorderLayout());
		// Add panels to the main panel
		preparePanels();
		
		// Add listeners
		listener = new ButtonListener();
		newGameMenuItem.addActionListener(listener);
		undoMenuItem.addActionListener(listener);
		quitMenuItem.addActionListener(listener);
		rulesMenuItem.addActionListener(listener);
		aboutMenuItem.addActionListener(listener);
		
		for (int r = 0; r < size; r++) {
			for (int c = 0; c < size; c++) {
				board[r][c].addActionListener(listener);
			}
		}
		
		// Start the countdown timer
		timer = new Countdown();
	}
	
	/*****************************************************
	 * Gets all the panels (board, turn, and score panel)
	 * and adds them to them to the main panel and frame.
	 ****************************************************/
	private void preparePanels() {
		this.removeAll();
		this.add(getBoardPanel(), BorderLayout.NORTH);
		this.add(getTurnPanel(), BorderLayout.CENTER);
		this.add(getScorePanel(), BorderLayout.SOUTH);
		frame.getContentPane().removeAll();
		frame.getContentPane().add(this);
		frame.pack();
	}
	
	/*********************************************************
	 * Makes the board panel containing the board of JButtons
	 * and adds actionListener to each of them then returns
	 * the panel.
	 * 
	 * @return the board panel
	 ********************************************************/
	private JPanel getBoardPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(size, size, 2, 2));
		board = new JButton[size][size];
		
		for (int row = 0; row < size; row++) {
			for (int col = 0; col < size; col++) {
				board[row][col] = new JButton("");
				board[row][col].setPreferredSize(new Dimension(30, 30));
				board[row][col].setBorder(new LineBorder(Color.black, 1));
				board[row][col].addActionListener(listener);
				panel.add(board[row][col]);
			}
		}
		
		return panel;
	}
	
	/*******************************************************
	 * Makes the turn panel that signifies whose turn it is
	 * and then returns the panel.
	 * 
	 * @return the turn panel
	 *******************************************************/
	private JPanel getTurnPanel() {
		JPanel panel = new JPanel();
		turnLabel = new JLabel("It is Player " + turn + "'s turn.");
		panel.add(turnLabel);
		
		return panel;
	}
	
	/*********************************************************
	 * Makes the score panel containing sub-panels with the 
	 * labels that track the amount of times the players have
	 * won and then returns the score panel.
	 * 
	 * @return the score panel
	 *********************************************************/
	private JPanel getScorePanel() {
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		
		panel1.setLayout(new BorderLayout());
		
		// Player 1 and 2 panel
		p1WinsLabel = new JLabel("Player 1 Wins: " + p1Wins);
		panel2.add(p1WinsLabel);
		panel2.add(Box.createRigidArea(new Dimension(10,0)));
		p2WinsLabel = new JLabel("Player 2 Wins: " + p2Wins);
		panel2.add(p2WinsLabel);
		
		// Player 3 and 4 panel
		if (players >= 3) {
			p3WinsLabel = new JLabel("Player 3 Wins: " + p3Wins);
			panel3.add(p3WinsLabel);
		}
		if (players == 4) {
			panel3.add(Box.createRigidArea(new Dimension(10,0)));
			p4WinsLabel = new JLabel("Player 4 Wins: " + p4Wins);
			panel3.add(p4WinsLabel);
		}
		
		// Add Player 1 and 2 panel
		panel1.add(panel2, BorderLayout.NORTH);
		// Add Player 3 and 4 panel if needed
		if (players > 2) {
			panel1.add(panel3, BorderLayout.SOUTH);
		}

		return panel1;
	}
	
	/***********************************************************
	 * Ask the user the size of the board, from 3 to 20,
	 * setting 3 as the default in the text box. It then returns
	 * that size.
	 * 
	 * @return size of board
	 **********************************************************/
	private int getBoardSize() {
		int size = BOARD_SIZE_MIN - 1;
		String input = "";
		while (size < BOARD_SIZE_MIN || size > BOARD_SIZE_MAX) {
			try {
				input = JOptionPane.showInputDialog(null,
					    "Enter board size [" + BOARD_SIZE_MIN + " - "
					    + BOARD_SIZE_MAX + "]:", BOARD_SIZE_MIN);
				if (input == null) {
					System.exit(0);
				}
				size = Integer.parseInt(input);
			} catch (Exception ex) {}
		}
		
		return size;
	}
	
	/*****************************************************
	 * Ask the user how many players are playing the game,
	 * from 2 to 4, with 2 as the default in the text box.
	 * It then returns the amount of players.
	 * 
	 * @return amount of players
	 *****************************************************/
	private int getAmountOfPlayers() {
		int players = PLAYERS_MIN - 1;
		String input = "";
		while (players < PLAYERS_MIN || players > PLAYERS_MAX) {
			try {
				input = JOptionPane.showInputDialog(null,
						"How many players [" + PLAYERS_MIN + " - "
						+ PLAYERS_MAX + "]:", PLAYERS_MIN);
				if (input == null) {
					System.exit(0);
				}
				players = Integer.parseInt(input);
			} catch (Exception ex) {}
		}
		
		return players;
	}
	
	/**********************************************
	 * Ask the user which player will go first with
	 * player 1 as the default in the text box and
	 * then returns that player number.
	 * 
	 * @return player number that goes first
	 *********************************************/
	private int whoStarts() {
		int turn = 0;
		String input = "";
		while (turn < (PLAYERS_MIN - 1) || turn > players) {
			try {
				input = JOptionPane.showInputDialog(null,
						"What player starts? [" + (PLAYERS_MIN - 1) + " - "
						+ players + "]:", (PLAYERS_MIN - 1)) ;
				if (input == null) {
					System.exit(0);
				}
				turn = Integer.parseInt(input);
			} catch (Exception ex) {}
		}
		
		return turn;
	}
	
	/*******************************************************
	 * Sets the turn variable in the SurroundPanel class
	 * to match the turn variable of the SurroundGame class
	 * then updates the turn labels accordingly.
	 ******************************************************/
	private void setTurnAndLabel() {
		turn = game.getTurn();
		turnLabel.setText("It is Player " + turn + "'s turn.");
	}
	
	/*************************************************
	 * Resets the board and instance of SurroundGame.
	 ************************************************/
	private void boardReset() {
		game.reset();
		setTurnAndLabel();
		undoMenuItem.setEnabled(false);
		timer.resetTimer();
		
		for (int r = 0; r < size; r++) {
			for (int c = 0; c < size; c++) {
				board[r][c].setText("");
			}
		}
	}
	
	/*************************************************
	 * Goes through the board to see if all the spots
	 * are empty, signifying no moves have been made.
	 * 
	 * @return true if empty, false otherwise
	 ************************************************/
	private Boolean isBoardEmpty() {
		for (int r = 0; r < size; r++) {
			for (int c = 0; c < size; c++) {
				if (board[r][c].getText() != "") {
					return false;
				}
			}
		}
		
		return true;
	}
	
	/*********************************************************
	 * Gives each player their own color and sets the JButton
	 * text of the cells that the player picks to their
	 * assigned color.
	 * 
	 * @param row row of the selected button
	 * @param col column of the selected button
	 * @param player what player made the move
	 ********************************************************/
	private void setPlayerColor(int row, int col, int player) {
		if (player == 1) {
			board[row][col].setForeground(Color.RED);
		}
		if (player == 2) {
			board[row][col].setForeground(Color.BLUE);
		}
		if (player == 3) {
			board[row][col].setForeground(Color.MAGENTA.darker());
		}
		if (player == 4) {
			board[row][col].setForeground(Color.GREEN.darker());
		}
	}
	
	/*****************************************************
	 * Represents a countdown timer that only gives the
	 * player so long to make a move before it moves onto
	 * the next player.
	 ****************************************************/
	private class Countdown {
		/** Sets delay of timer to 15 seconds */
		private static final int DELAY = 15000;
		private Timer t;
		
		ActionListener countdown = new ActionListener() {
			
			/***************************************************
			 * Action to be performed once the timer reaches 0.
			 * Sets turn to next player, sets the turn label,
			 * and restarts the timer.
			 **************************************************/
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Time's up, your turn has been skipped", 
											  "Time's up!", JOptionPane.WARNING_MESSAGE);  
				game.nextPlayer();
				setTurnAndLabel();
		        t.restart();
		    }
		};
		
		/******************************************************
		 * Constructs a Countdown object and starts the timer.
		 *****************************************************/
		private Countdown() {
			t = new Timer(DELAY, countdown);
			t.start();
		}
		
		/******************************
		 * Resets the countdown timer.
		 *****************************/
		public void resetTimer() {
			t.restart();
		}
		
		/******************************
		 * Pauses the countdown timer.
		 *****************************/
		public void pauseTimer() {
			t.stop();
		}
		
		/******************************
		 * Unpauses the countdown timer.
		 *****************************/
		public void unpauseTimer() {
			t.start();
		}
	}
	
	/*******************************************************
	 * Private class to check which button has been pressed
	 * and what actions to perform upon that button being
	 * pressed.
	 ******************************************************/
	private class ButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			// New game button pressed
			if (e.getSource() == newGameMenuItem) {
				size = getBoardSize();
				players = getAmountOfPlayers();
				turn = whoStarts();
				
				game = new SurroundGame(size, players, turn);
				
				preparePanels();
				timer.resetTimer();
				return;
			}
			
			// Quit button pressed
			if (e.getSource() == quitMenuItem) {
				System.exit(0);
			}
			
			// Undo button pressed
			if (e.getSource() == undoMenuItem) {
				Point temp = game.undo();
				board[temp.x][temp.y].setText("");
				setTurnAndLabel();
				if (isBoardEmpty()) {
					undoMenuItem.setEnabled(false);
				}
				timer.resetTimer();
				return;
			}
			
			// Rules button pressed
			// Gives the rules of the game
			if (e.getSource() == rulesMenuItem) {
				timer.pauseTimer();
				JOptionPane.showMessageDialog(null, 
						
						"Goal of the game: Surround your opponent's cell with your own. \n"
						+ "Ways to surround your opponent: \n"
						+ "Surround North, South, East, and West of your opponents. (See Ex. 1) \n"
						+ "Surround 3 sides of your opponent if they're on the side. (See Ex. 2) \n"
						+ "Surround 2 sides of your opponent if they're in the corner. (See Ex. 3) \n \n"
						+ "** Player 2 wins all of the examples except Ex. 5. ** \n"
						+ "** 0's represent empty cells in these examples. ** \n\n"
						+ "Ex. 1                   Ex. 2                   Ex. 3 \n"
						+ "[0][0][0][0][0] \t \t [0][0][0][0][0] \t \t [1][2][0][0][0] \n"
						+ "[0][0][2][0][0] \t \t [0][0][0][0][2] \t \t [2][0][0][0][0] \n"
						+ "[0][2][1][2][0] \t \t [0][0][0][2][1] \t \t [0][0][0][0][0] \n"
						+ "[0][0][2][0][0] \t \t [0][0][0][0][2] \t \t [0][0][0][0][0] \n"
						+ "[0][0][0][0][0] \t \t [0][0][0][0][0] \t \t [0][0][0][0][0] \n \n"
						+ "If there are more than two players, the one that places the last \n"
						+ "cell to surround the opponent gets the win. (See Ex. 4) \n \n"
						+ "Ex. 4 \n"
						+ "[0][0][0][0][0] \n"
						+ "[0][0][2][0][0] \n"
						+ "[0][4][1][3][0] \t Player 2 wins for placing the last cell. \n"
						+ "[0][0][4][0][0] \n"
						+ "[0][0][0][0][0] \n \n"
						+ "You can not lose by surrounding yourself. (See Ex. 5) \n \n"
						+ "Ex. 5 \n"
						+ "[0][0][0][0][0] \n"
						+ "[0][0][2][0][0] \n"
						+ "[0][1][1][3][0] \t Player 1 is not out, and game is still in progress. \n"
						+ "[0][0][4][0][0] \n"
						+ "[0][0][0][0][0]",
						
						"Rules", JOptionPane.INFORMATION_MESSAGE);
				timer.unpauseTimer();
				return;
			}
			
			// About button pressed
			if (e.getSource() == aboutMenuItem) {
				timer.pauseTimer();
				JOptionPane.showMessageDialog(null, "Surround \n by Kyle Hekhuis \n Version 1.0",
						"About", JOptionPane.INFORMATION_MESSAGE);
				timer.unpauseTimer();
				return;
			}
			
			// Determine which button was selected
			for (int r = 0; r < size; r++) {
				for (int c = 0; c < size; c++) {
					if (e.getSource() == board[r][c]) {
						if (game.select(r, c)) {
							game.setCell(r, c);
							setPlayerColor(r, c, turn);
							board[r][c].setText(Integer.toString(turn));
							setTurnAndLabel();
							undoMenuItem.setEnabled(true);
							timer.resetTimer();
						} else {
							timer.pauseTimer();
							JOptionPane.showMessageDialog(null, "Pick again.");
							timer.unpauseTimer();
						}
					}
				}
			}
			
			game.isWinner();
			
			// Check if Player 1 won
			if (game.getStatus() == GameStatus.PLAYER1_WON) {
				timer.pauseTimer();
				JOptionPane.showMessageDialog(null,"Player 1 won! \n The game will reset.");
				boardReset();
				p1Wins++;
				p1WinsLabel.setText("Player 1 Wins: " + p1Wins);
				return;
			}
			
			// Check if Player 2 won
			if (game.getStatus() == GameStatus.PLAYER2_WON) {
				timer.pauseTimer();
				JOptionPane.showMessageDialog(null,"Player 2 won! \n The game will reset.");
				boardReset();
				p2Wins++;
				p2WinsLabel.setText("Player 2 Wins: " + p2Wins);
				return;
			}
			
			// Check if Player 3 won
			if (game.getStatus() == GameStatus.PLAYER3_WON) {
				timer.pauseTimer();
				JOptionPane.showMessageDialog(null,"Player 3 won! \n The game will reset.");
				boardReset();
				p3Wins++;
				p3WinsLabel.setText("Player 3 Wins: " + p3Wins);
				return;
			}
			
			// Check if Player 4 won
			if (game.getStatus() == GameStatus.PLAYER4_WON) {
				timer.pauseTimer();
				JOptionPane.showMessageDialog(null,"Player 4 won! \n The game will reset.");
				boardReset();
				p4Wins++;
				p4WinsLabel.setText("Player 4 Wins: " + p4Wins);
				return;
			}
			
			// Check if the game ended in a tie
			if (game.getStatus() == GameStatus.TIE) {
				timer.pauseTimer();
				JOptionPane.showMessageDialog(null,"Tie game! \n The game will reset.");
				boardReset();
				return;
			}
		} // End of actionPerformed()
	} // End of ButtonListener
}
