package project2;

/**************************************************
 * Represents a Cell on the game board that
 * contains the player's number who's in that Cell.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 *************************************************/
public class Cell {

	/** 0 Represents empty spot, >0 represents players */
	private int playerNumber;
	
	/*************************************************
	 * Constructs a new Cell object with playerNumber 
	 * 0, representing an empty spot on the board.
	 ************************************************/
	public Cell() {
		playerNumber = 0;
	}
	
	/****************************************
	 * Returns the playerNumber of this Cell.
	 * 
	 * @return the players's number
	 ***************************************/
	public int getPlayerNumber() {
		return playerNumber;
	}

	/***************************************************
	 * Sets the playerNumber of this Cell.
	 * 
	 * @param playerNumber number to set as playerNumber
	 **************************************************/
	public void setPlayerNumber(int playerNumber) {
		this.playerNumber = playerNumber;
	}
}
